﻿using Envision.Tanks.Math;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Envision.Tanks
{
    public class Projectile
    {
        public Image projectile;
        public Vector2 position;
        public Vector2 direction;
        public int damage;
        private Vector2 gravity = new Vector2(0, 9.81f);
        private float angle;
        private float power;

        public Projectile(Image projectileImage, Vector2 startPosition, float power, int damage, int angle)
        {
            projectile = projectileImage;
            position = startPosition;
            direction = new Vector2((float)Util.Cos(angle - 90), (float)Util.Sin(angle - 90)).Normalized;
            this.damage = damage;
            this.angle = angle;
            this.power = power;
        }

        private void UpdatePosition(DateTime m_LastUpdateTime)
        {
            direction += gravity * Convert.ToSingle((DateTime.Now - m_LastUpdateTime).Milliseconds) / 1000;
            position += direction * power;
            angle = Util.RadiansToDegrees((float)System.Math.Atan2(direction.Normalized.Y, direction.Normalized.X)) + 90;
        }

        public void PaintProjectile(PaintEventArgs e, DateTime m_LastUpdateTime, int player)
        {
            UpdatePosition(m_LastUpdateTime);
            e.Graphics.TranslateTransform(position.X, position.Y);
            e.Graphics.RotateTransform(angle);
            e.Graphics.DrawImage(projectile, 0, 0);
        }
    }
}
