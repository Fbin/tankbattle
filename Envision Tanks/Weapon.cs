﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Envision.Tanks
{
    public class WeaponDesc
    {
        public int burstAmount { get; set; }
        public int shotsAmount { get; set; }
        public float minVel { get; set; }
        public float maxVel { get; set; }
        public int damage { get; set; }
        public string projectileImage { get; set; }
    }

    [XmlRootAttribute("Weapons")]
    public class WeaponDescCollection
    {
        [XmlElement("Weapon")]
        public WeaponDesc[] WeaponDescs { get; set; }
    }

    public class Weapon
    {
        static private List<Weapon> m_aWeapns;

        public WeaponDesc description;
        public int ammo;

        public static void LoadWeapons()
        {
            m_aWeapns = new List<Weapon>();
            using (TextReader reader = new StreamReader("Resources\\weapons.xml"))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(WeaponDescCollection));
                WeaponDescCollection col =(WeaponDescCollection)serializer.Deserialize(reader);
                foreach(WeaponDesc desc in col.WeaponDescs)
                {
                    Weapon w = new Weapon();
                    w.description = desc;
                    w.Reload();
                    m_aWeapns.Add(w);
                }
            }

        }
        public static Weapon GetWeapon(int index)
        {
            return m_aWeapns[index];
        }

        public int GetWeaponCount()
        {
            return m_aWeapns.Count;
        }

        public Weapon()
        {

        }

        public void Reload() { ammo = description.shotsAmount; }

        public void PaintWeapon(PaintEventArgs e, int currentWeaponIndex)
        {
            var w = e.Graphics.MeasureString("Current Weapon:", new Font("Arial", 16));
            var v = e.Graphics.MeasureString("->", new Font("Arial", 16));
            for (int i = 0; i<m_aWeapns.Count - 1; i++)
            {
                e.Graphics.DrawImage(Image.FromFile(m_aWeapns[currentWeaponIndex * ((i + 1) % m_aWeapns.Count)].description.projectileImage), 10 + w.Width + i * (v.Width + 20), 10, 20, 20);
                e.Graphics.DrawString("->", new Font("Arial", 16), new SolidBrush(Color.White), 30 + w.Width + i * (v.Width + 20), 4);
                e.Graphics.DrawString("<-", new Font("Arial", 16), new SolidBrush(Color.White), 30 + w.Width + i * (v.Width + 20), 16);
            }
            e.Graphics.DrawImage(Image.FromFile(m_aWeapns[(currentWeaponIndex + m_aWeapns.Count - 1) % m_aWeapns.Count].description.projectileImage), 10 + w.Width + (m_aWeapns.Count - 1) * (v.Width + 20), 10, 20, 20);
        }
    }
}
