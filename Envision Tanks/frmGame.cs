﻿using Envision.Tanks.Math;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Envision.Tanks
{
    public partial class frmGame : Form
    {
        public frmGame()
        {
            InitializeComponent();
            Random rn = new Random();
            Random rand = new Random(rn.Next(0, 1000));
            //area = RandomTerrain(Width, rand.Next(0, 100), rand.Next(0, 100), 100, 100, 5);
            area = RandomTerrain(Width, Height/2, 5);

            m_Font = new Font("Arial", 16);
            m_FontBrush = new SolidBrush(Color.White);

            m_Timer = new Timer();
            m_Timer.Interval = (int)(1f / 30f * 1000f);
            m_Timer.Enabled = true;
            m_Timer.Tick += OnNextFrame;

            this.DoubleBuffered = true;
            this.KeyPreview = true;

            StartGame();
        }

        public void StartGame()
        {
            Weapon.LoadWeapons();
            currentShot = new List<Projectile>();
            players = new List<Player>();
            players.Add(new Player(new Vector2(Width / 4 + 25, Height - Convert.ToSingle(area[Width / 4 + 25]) - 50), Image.FromFile("Resources\\green_tank.png"), new Vector2(23, 30), Image.FromFile("Resources\\green_cannon.png")));
            players.Add(new Player(new Vector2(3 * Width / 4 + 25, Height - 50 - Convert.ToSingle(area[3 * Width / 4 + 25])), Image.FromFile("Resources\\red_tank.png"), new Vector2(23, 30), Image.FromFile("Resources\\red_cannon.png")));
            power = players[0].currentWeapon.description.minVel;

            m_Timer.Start();
            this.KeyUp += FrmGame_KeyUp;
        }

        private void OnNextFrame(object sender, EventArgs e)
        {
            //Debug.Print("-->" + this.KeyUp);

            var now = DateTime.Now;
            delta = now - m_LastUpdateTime;
            m_LastUpdateTime = now;

            // update logic objects here
            tTime += Convert.ToSingle(delta.Milliseconds)/1000;
            if(tTime>30)
            {
                UpdateCurrentPlayer();
            }

            this.Invalidate(); // invalidate form to trigger repaint
        }

        private void frmGame_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.LightSkyBlue);
            
            // render here

            // draw text
            var size = e.Graphics.MeasureString("Envision Tanks", m_Font);
            e.Graphics.DrawString("Envision Tanks", m_Font, m_FontBrush, Width / 2 - size.Width / 2, 10);
            var m = e.Graphics.MeasureString("Power", m_Font);
            e.Graphics.DrawString("Power", m_Font, m_FontBrush, Width / 2 - size.Width, 50);
            e.Graphics.DrawString(System.Math.Floor(power).ToString(), m_Font, m_FontBrush, Width / 2 - size.Width / 2, 50);
            e.Graphics.DrawString("Angle", m_Font, m_FontBrush, Width / 2, 50);
            e.Graphics.DrawString(players[currentPlayer].angle.ToString(), m_Font, m_FontBrush, Width / 2 + 70, 50);
            var l = e.Graphics.MeasureString("Current Player", m_Font);
            e.Graphics.DrawString("Current Player", m_Font, m_FontBrush, Width - 400, 10);
            e.Graphics.DrawString((currentPlayer + 1).ToString(), m_Font, m_FontBrush, Width - 400 + l.Width, 10);
            e.Graphics.DrawString("Remaining Time", m_Font, m_FontBrush, Width - 220, 10);
            e.Graphics.DrawString(System.Math.Floor(30 - tTime).ToString(), m_Font, m_FontBrush, Width - 50, 10);
            e.Graphics.DrawString("Current Weapon:", m_Font, m_FontBrush, 10, 10);
            players[currentPlayer].currentWeapon.PaintWeapon(e, players[currentPlayer].currentWeaponIndex);


            for (int i = 0; i<area.Count(); i++)
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.ForestGreen), i, Height - Convert.ToSingle(area[i]), 2, Convert.ToSingle(area[i]));
            }

            if(currentShot.Count != 0)
            {
                e.Graphics.ResetTransform();
                if(players.Count != 1)
                {
                    int otherPlayer = (currentPlayer + 1) % 2;
                    Player other = players[otherPlayer];
                    for (int i = 0; i < currentShot.Count; i++)
                    {
                        if(currentShot[i] != null)
                        {
                            currentShot[i].PaintProjectile(e, m_LastUpdateTime, currentPlayer);
                            e.Graphics.ResetTransform();

                            if (currentShot[i].position.X <= 0)
                            {
                                currentShot.RemoveAt(i);
                                UpdateCurrentPlayer();
                                return;
                            }
                            if (currentShot[i].position.X >= Width)
                            {
                                currentShot.RemoveAt(i);
                                UpdateCurrentPlayer();
                                return;
                            }
                            if (currentShot[i].position.Y <= 0)
                            {
                                currentShot.RemoveAt(i);
                                UpdateCurrentPlayer();
                                return;
                            }
                            if (currentShot[i].position.Y >= Height - area[(int)currentShot[i].position.X])
                            {
                                currentShot.RemoveAt(i);
                                UpdateCurrentPlayer();
                                return;
                            }
                            else if (currentShot[i].position.X >= other.position.X && currentShot[i].position.X <= other.position.X + other.playerImage.Width && currentShot[i].position.Y >= other.position.Y && currentShot[i].position.Y <= other.position.Y + other.playerImage.Height)
                            {
                                players[otherPlayer].health -= currentShot[i].damage;
                                currentShot.RemoveAt(i);
                                UpdateCurrentPlayer();
                            }
                        }
                    }
                }
            }

            for(int i = 0; i<players.Count; i++)
            {
                players[i].PaintPlayer(e, i, Width);
                e.Graphics.ResetTransform();
            }

            // draw game area
            e.Graphics.DrawRectangle(new Pen(Color.Blue), 0, 50, Width - 1, Height - 50);

            if(gameEnd)
            {
                var w = e.Graphics.MeasureString("Player" + winner.ToString() + " wins!", m_Font);
                e.Graphics.DrawString("Player " + winner.ToString() + " wins!", m_Font, m_FontBrush, Width / 2 - w.Width / 2, Height / 2 - w.Height / 2);
            }
        }

        private Timer m_Timer;
        private DateTime m_LastUpdateTime;
        private TimeSpan delta;

        private Font m_Font;
        private SolidBrush m_FontBrush;

        private float power;
        private int currentPlayer = 0;
        private float tTime = 0;
        private bool gameEnd;
        private int winner;
        private double[] area;

        public List<Player> players;
        public List<Projectile> currentShot;

        private void UpdateCurrentPlayer()
        {
            if(players[currentPlayer].health>0)
            {
                currentPlayer++;
                currentPlayer %= 2;
                power = players[currentPlayer].currentWeapon.description.minVel;
                tTime = 0;
            }
            else
            {
                gameEnd = true;
                players.RemoveAt(currentPlayer);
                if(currentPlayer == 1)
                {
                    currentPlayer++;
                    currentPlayer %= 2;
                    winner = 1;
                }
                else
                {
                    winner = 2;
                }
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if(gameEnd)
            {
                Application.Exit();
            }
            else
            {
                if(keyData == Keys.W)
                {
                    gameEnd = true;
                    players.RemoveAt(currentPlayer);
                    if (currentPlayer == 1)
                    {
                        currentPlayer++;
                        currentPlayer %= 2;
                        winner = 1;
                    }
                    else
                    {
                        winner = 2;
                    }
                }
                if (keyData == Keys.Up)
                {
                    players[currentPlayer].angle++;
                    players[currentPlayer].angle = Util.Clamp(players[currentPlayer].angle, 0, 180);
                    return true;
                }
                if (keyData == Keys.Down)
                {
                    players[currentPlayer].angle--;
                    players[currentPlayer].angle = Util.Clamp(players[currentPlayer].angle, 0, 180);
                    return true;
                }
                if(keyData==Keys.Space)
                {
                    if (players[currentPlayer].currentWeapon.ammo <= -1 || players[currentPlayer].currentWeapon.ammo > 0)
                    {
                        power += Convert.ToSingle(delta.Milliseconds) / 1000;
                        power = System.Math.Min(power, players[currentPlayer].currentWeapon.description.maxVel);
                    }
                    return true;
                }

                if(keyData == Keys.Right)
                {
                    players[currentPlayer].currentWeaponIndex++;
                    players[currentPlayer].currentWeaponIndex %= players[currentPlayer].currentWeapon.GetWeaponCount();
                    players[currentPlayer].currentWeapon = Weapon.GetWeapon(players[currentPlayer].currentWeaponIndex);
                    power = players[currentPlayer].currentWeapon.description.minVel;
                }
                if (keyData == Keys.Left)
                {
                    players[currentPlayer].currentWeaponIndex--;
                    if(players[currentPlayer].currentWeaponIndex<0)
                    {
                        players[currentPlayer].currentWeaponIndex += players[currentPlayer].currentWeapon.GetWeaponCount();
                    }
                    players[currentPlayer].currentWeapon = Weapon.GetWeapon(players[currentPlayer].currentWeaponIndex);
                    power = players[currentPlayer].currentWeapon.description.minVel;
                }
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void FrmGame_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space && currentShot.Count == 0)
            {
                if(players[currentPlayer].currentWeapon.ammo <= -1 || players[currentPlayer].currentWeapon.ammo > 0)
                {
                    if (currentPlayer == 0)
                    {
                        for(int i = 0; i<players[currentPlayer].currentWeapon.description.burstAmount; i++)
                        {
                            currentShot.Add(new Projectile(Image.FromFile(players[currentPlayer].currentWeapon.description.projectileImage), players[currentPlayer].GetShotPosition(0), power, players[currentPlayer].currentWeapon.description.damage, -players[currentPlayer].angle + 90 + (int)(30*Util.Sin(i*90 - 90))));
                            players[0].currentWeapon.ammo--;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < players[currentPlayer].currentWeapon.description.burstAmount; i++)
                        {
                            currentShot.Add(new Projectile(Image.FromFile(players[currentPlayer].currentWeapon.description.projectileImage), players[currentPlayer].GetShotPosition(1), power, players[currentPlayer].currentWeapon.description.damage, players[currentPlayer].angle - 90 + (int)(30 * Util.Sin(i * 90 - 90))));
                            players[1].currentWeapon.ammo--;
                        }
                    }
                }
            }
        }

        private static Random rnd = new Random();
        private double[] RandomTerrain(int length, int sinuses, int cosinuses, double amplsin, double amplcos, double noise)
        {
            double[] returnValues = new double[length];
            for (int i = 0; i < length; i++)
            {
                for (int sin = 1; sin <= sinuses; sin++)
                {
                    returnValues[i] += amplsin * Util.Sin((2 * sin * i * System.Math.PI) / (double)length);
                }
                for (int cos = 1; cos <= cosinuses; cos++)
                {
                    returnValues[i] += amplcos * Util.Cos((2 * cos * i * System.Math.PI) / (double)length);
                }
                returnValues[i] += (noise * rnd.NextDouble()) - (noise * rnd.NextDouble());
            }

            double ofs = returnValues.Min();
            if (ofs < 0)
            {
                ofs *= -1;
                for (int i = 0; i < length; i++)
                {
                    returnValues[i] += ofs;
                }
            }

            double max = returnValues.Max();
            if (max >= 100)
            {
                double scaler = max / 100.0;
                for (int i = 0; i < length; i++)
                {
                    returnValues[i] /= scaler;
                }
            }
            return returnValues;
        }

        private double[] RandomTerrain(int length, int maxVal, int numberOfSegments)
        {
            double[] result = new double[length+1];
            double[] newD;
            Random random = new Random();
            result[0] = Util.Sin(random.Next()) + 20;
            for (int i=0; i<numberOfSegments; i++)
            {
                Random rand = new Random();
                newD = new double[(i + 1) * length / numberOfSegments - i * length / numberOfSegments];
                float val = rand.Next(0, 101)/100f;
                int myv = i * length / numberOfSegments;
                if (val<0.75f)
                {
                    if(val<0.5f)
                    {
                        if(val<0.25f)
                        {
                            newD = GenerateLine(i * length / numberOfSegments, (i + 1) * length / numberOfSegments, result[i * length / numberOfSegments], maxVal, false);
                            newD.CopyTo(result, i * length / numberOfSegments + 1);
                            continue;
                        }
                        newD = GenerateCurve(i * length / numberOfSegments, (i + 1) * length / numberOfSegments, result[i * length / numberOfSegments], maxVal, false);
                        newD.CopyTo(result, i * length / numberOfSegments + 1);
                        continue;
                    }
                    newD = GenerateCurve(i * length / numberOfSegments, (i + 1) * length / numberOfSegments, result[i * length / numberOfSegments], maxVal, true);
                    newD.CopyTo(result, i * length / numberOfSegments + 1);
                    continue;
                }
                newD = GenerateLine(i * length / numberOfSegments, (i + 1) * length / numberOfSegments, result[i * length / numberOfSegments], maxVal, true);
                newD.CopyTo(result, i * length / numberOfSegments + 1);
            }
            return result;
        }

        private double[] GenerateLine(int startPoint, int endPoint, double startValue, int maxVal, bool growing)
        {
            double[] result = new double[endPoint - startPoint];
            Random random = new Random();
            double endValue;
            if(growing)
            {
                endValue = random.Next((int)startValue, maxVal);
            }
            else
            {
                endValue = random.Next(10, (int)startValue);
            }
            for(int i = startPoint; i<endPoint; i++)
            {
                result[i - startPoint] = (endValue * (i - startPoint) - startValue * (i - endPoint)) / (endPoint - startPoint);
            }
            return result;
        }

        private double[] GenerateCurve(int startPoint, int endPoint, double startValue, int maxVal, bool growing)
        {
            double[] result = new double[endPoint - startPoint];
            Random randomCurve = new Random();
            float endValue;
            if (growing)
            {
                endValue = randomCurve.Next((int)startValue, maxVal);
            }
            else
            {
                endValue = randomCurve.Next(10, (int)startValue);
            }
            randomCurve = new Random();
            float val = randomCurve.Next(0, 101) / 100f;
            if(val<0.5f)
            {
                for(int i = startPoint; i<endPoint; i++)
                {
                    if(growing)
                    {
                        result[i- startPoint] = Util.Sin((float)(i - startPoint) / (float)(endPoint - startPoint) * 180 - 90) * (endValue - startValue) / 2 + (endValue + startValue) / 2;
                    }
                    else
                    {
                        result[i - startPoint] = Util.Sin((float)(i - startPoint) / (float)(endPoint - startPoint) * 180 + 90) * (startValue - endValue) / 2 + (endValue + startValue) / 2;
                    }
                }
            }
            else
            {
                for (int i = startPoint; i < endPoint; i++)
                {
                    if(growing)
                    {
                        result[i - startPoint] = Util.Cos((float)(i - startPoint) / (float)(endPoint - startPoint) * 180 + 180) * (endValue - startValue) / 2 + (endValue + startValue) / 2;
                    }
                    else
                    {
                        result[i - startPoint] = Util.Cos((float)(i - startPoint) / (float)(endPoint - startPoint) * 180) * (startValue - endValue) / 2 + (startValue + endValue) / 2;
                    }
                }
            }
            return result;
        }
    }
}
