﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Envision.Tanks.Math
{
    public class Util
    {
        public static float DegreesToRadians(float degrees)
        {
            return (float)(degrees * (System.Math.PI / 180));
        }

        public static float RadiansToDegrees(float radians)
        {
            return (float)(radians * (180 / System.Math.PI));
        }

        public static int Clamp(int x, int min, int max)
        {
            if(x<min)
            {
                return min;
            }
            else if(x>max)
            {
                return max;
            }
            else
            {
                return x;
            }
        }

        public static double Sin(double degrees)
        {
            return System.Math.Sin(degrees * System.Math.PI / 180);
        }

        public static double Cos(double degrees)
        {
            return System.Math.Cos(degrees * System.Math.PI / 180);
        }
    }
}
