﻿using Envision.Tanks.Math;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Envision.Tanks
{
    public class Player
    {
        public int health;
        public Vector2 position;
        public Image playerImage;
        public int angle;
        public Vector2 cannonPosition;
        public Image cannonImage;
        public int currentWeaponIndex = 0;
        public Weapon currentWeapon;

        public Player(Vector2 position, Image playerImage, Vector2 cannonPosition, Image cannonImage)
        {
            health = 100;
            currentWeapon = Weapon.GetWeapon(currentWeaponIndex);
            this.position = position;
            this.playerImage = playerImage;
            angle = 30;
            this.cannonPosition = cannonPosition;
            this.cannonImage = cannonImage;
        }

        public void PaintPlayer(PaintEventArgs e, int player, int windowWidth)
        {
            if(player == 0)
            {
                e.Graphics.TranslateTransform(position.X, position.Y);
                e.Graphics.DrawImage(playerImage, 0, 0, 50, 50);
                e.Graphics.TranslateTransform(cannonPosition.X, cannonPosition.Y);
                e.Graphics.RotateTransform(-angle);
                e.Graphics.DrawImage(cannonImage, 0, 0, 32, 4);
                e.Graphics.ResetTransform();
                var n1 = e.Graphics.MeasureString("Player 1:", new Font("Arial", 16));
                e.Graphics.DrawString("Player 1:", new Font("Arial", 16), new SolidBrush(Color.White), 10, 50);
                e.Graphics.DrawString(health.ToString(), new Font("Arial", 16), new SolidBrush(Color.White), 10 + n1.Width, 50);                
            }
            else
            {
                e.Graphics.TranslateTransform(position.X, position.Y);
                e.Graphics.DrawImage(playerImage, 0, 0, 50, 50);
                e.Graphics.TranslateTransform(cannonPosition.X, cannonPosition.Y);
                e.Graphics.RotateTransform(angle - 180);
                e.Graphics.DrawImage(cannonImage, 0, 0, 32, 4);
                e.Graphics.ResetTransform();
                var n2 = e.Graphics.MeasureString("Player 2:", new Font("Arial", 16));
                var n3 = e.Graphics.MeasureString("100", new Font("Arial", 16));
                e.Graphics.DrawString("Player 2:", new Font("Arial", 16), new SolidBrush(Color.White), windowWidth - n2.Width - n3.Width - 10, 50);
                e.Graphics.DrawString(health.ToString(), new Font("Arial", 16), new SolidBrush(Color.White), windowWidth - n3.Width - 10, 50);
            }
        }

        public Vector2 GetShotPosition(int player)
        {
            if(player == 0)
            {
                return position + cannonPosition + new Vector2((float)Util.Cos(-angle) * 32 - (float)Util.Sin(-angle), ((float)Util.Cos(-angle) + (float)Util.Sin(-angle) * 32));
            }
            else
            {
                return position + cannonPosition + new Vector2((float)Util.Cos(angle - 180) * 32 - (float)Util.Sin(angle - 180), ((float)Util.Cos(angle - 180) + (float)Util.Sin(angle - 180) * 32));
            }
        }
    }
}
